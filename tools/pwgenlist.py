import itertools
import argparse
import datetime
import sys
import os
import re

def main():
	# argparse ftw
	parser = argparse.ArgumentParser(description='Easiest WPA password generator')
	reqgroup = parser.add_argument_group('required arguments')
	reqgroup.add_argument('-i','--input', required=True, type=str, nargs='+', metavar='FOO,BAR', help="Comma separated strings of possible combinations. Ex: -i foo,bar raw,nobar")
	parser.add_argument('-s', '--symbols', action='store_true', help="Include common symbols on common places")
	parser.add_argument('-n', '--numbers', action='store_true', help="Include common numbers on the common places (such as 2018, 123, 12345)")
	parser.add_argument('-l', '--level', type=int, default=1, choices=(1,2,3), metavar='N', help="Password level of complexity, 1 = only lower+upper, 2 = lower+upper+capital, 3 = lower+upper+capital+hackerize")
	parser.add_argument('-o', '--output', type=str, metavar='OUT', help="Output file where to save the wordlist (default: stdout)")
	args = parser.parse_args(sys.argv[1:])

	wf = None
	ostd = sys.stdout
	if not args.output is None:
		wf = open(args.output, 'w')
		sys.stdout = wf
	generate_wordlist(args)
	if not wf is None:
		sys.stdout = ostd
		wf.close()

# main generator function
# basically, every print(something) will be saved to
# either the output file, or the stdout
def generate_wordlist(args):
	def rnd_symbols(w):
		for r in '@$?%&#':
			if '@' in w:
				print(w.replace('@',r))
	for it in args.input:
		pms = []
		bcs = []
		for word in it.split(','):
			bcs.append(generate_base_words(word, args.level))
		nums = get_common_numbers() + get_lastnyears(20) + get_numrange(100)
		for it in itertools.product(*bcs):
			it = list(it)
			opts = []
			for cit in itertools.permutations(it,len(it)):
				opts.append(cit)
			if args.symbols or args.numbers:
				oit = it.copy()
				if args.numbers:
					li = oit.copy()
					li.append('#'); it.append('#')
					for cit in itertools.permutations(li,len(li)):
						opts.append(cit)
				if args.symbols:
					li = oit.copy()
					li.append('@'); it.append('@')
					for cit in itertools.permutations(li,len(li)):
						opts.append(cit)
				for cit in itertools.permutations(it,len(it)):
					opts.append(cit)
			opts = sorted(set(opts))
			for it in opts:
				cit = ''.join(it)
				if '#' in it:
					for n in nums:
						cc = cit
						cc = cc.replace('#',str(n))
						print(cc.replace('@',''))
						rnd_symbols(cc)
				if '@' in it and not '#' in it:
					rnd_symbols(cit)

# gets combinations with replacement
# of 0,1 for the given word, to replace later
def true_false_combinations(word):
	a = False; pm = []
	for c in word:
		a = not a; pm.append(not a)
	return itertools.combinations_with_replacement(pm,len(word))

# generate base words for given word,
# with the given complexity level
def generate_base_words(word, clevel):
	bc = []
	bc.append(word.lower())
	bc.append(word.upper())
	if clevel >= 2:
		bc += generate_all_capitalizations(word)
	if clevel >= 3:
		nw = []
		for b in bc:
			nw += generate_all_hackerization(b)
		nw = sorted(set(nw))
		bc += nw
	return sorted(set(bc))

# generate all possibilities with capital letters
# with the given word, combining all with replacements
def generate_all_capitalizations(word):
	rs = []
	for bls in true_false_combinations(word):
		w = word.lower(); i = 0
		for b in bls:
			if b:
				w = w[:i] + w[i].upper() + w[i+1:]
			i += 1
		rs.append(w)
	return sorted(set(rs))

# generate all hackerization possibilities with given word
# hackerization means replacing: A = 4, S = 5, E = 3, I = 1
def generate_all_hackerization(word):
	def hackerize(c):
		cl = c.lower()
		mp = {'a':4,'s':5,'e':3,'i':1,'o':0}
		return str(mp[cl]) if cl in mp else c
	rs = []
	for bls in true_false_combinations(word):
		w = word; i = 0
		for b in bls:
			if b:
				w = w[:i] + hackerize(w[i]) + w[i+1:]
			i += 1
		rs.append(w)
	return rs

# common numbers used on password
def get_common_numbers():
	return ['123','1234','135','12345','123456','1234567890','098','09876','0987654321']

# get the last N years in time
def get_lastnyears(n):
	y = datetime.datetime.now().year
	ys = []
	ys.append(y+1)
	for i in range(0,n):
		ys.append(y-i)
	return ys

# get a number range from 0
def get_numrange(ln):
	ns = []
	for i in range(0,ln):
		ns.append(i)
	return ns

if __name__ == '__main__':
	try:
		main()
	except BrokenPipeError:
		pass
